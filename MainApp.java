import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainApp {
    public static void main(String[] args) {
        //read the data from the Activities.txt file
        String fileName = "C:\\ptTema5\\Activities.txt";
        List<String> list = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            list = stream.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // creates the ArrayList containing the MonitoredData objects read from file
        List<MonitoredData> listMonitoredData = SolveTasks.buildMonitoredDataList(list);
        int taskIndex = 1;
        PrintWriter finalWriter = SolveTasks.createFileWriter(taskIndex);
        for (MonitoredData newItem: listMonitoredData) {
            finalWriter.println("Activity:");
            finalWriter.println("Start time: " + newItem.getStartTime() + "\n" + "End time: " + newItem.getEndTime() + "\n" + "Activity label: " + newItem.getActivityLabel() + "\n");
        }
        finalWriter.close();

        //task 2
        taskIndex++;
        PrintWriter finalWriter2 = SolveTasks.createFileWriter(taskIndex);
        finalWriter2.println("Nr of distinct days in the monitoring data: " + SolveTasks.nrDifferentDays(listMonitoredData));
        finalWriter2.close();

        //task 3
        taskIndex++;
        PrintWriter finalWriter3 = SolveTasks.createFileWriter(taskIndex);
        Map<String, Integer> activityOccurrenceOverMonitoringTime = SolveTasks.activityOccurrence(listMonitoredData);
        Set<Map.Entry<String, Integer>> setActivity = activityOccurrenceOverMonitoringTime.entrySet(); // from map to list
        finalWriter3.println("Number of occurrences for each activity over the entire monitoring time:");
        setActivity.forEach(finalWriter3::println);
        finalWriter3.close();

        //task 4
        taskIndex++;
        PrintWriter finalWriter4 = SolveTasks.createFileWriter(taskIndex);
        Map<Integer, Map<String, Integer>> activityOccurrenceWithinADay = SolveTasks.dailyAppearanceActivity(listMonitoredData);
        Set<Map.Entry<Integer, Map<String, Integer>>> secondSetActivity = activityOccurrenceWithinADay.entrySet(); // from map to list
        secondSetActivity.forEach(finalWriter4::println);
        finalWriter4.close();

        //task 5
        taskIndex++;
        PrintWriter finalWriter5 = SolveTasks.createFileWriter(taskIndex);
        Map<String, Integer> durationsMap = SolveTasks.getIntFromSeconds(listMonitoredData);
        finalWriter5.println("Total duration of each activity over the entire monitoring period:\n");
        durationsMap.forEach((name, duration) ->
            finalWriter5.println("Activity: " + name + " - Duration: " + duration)
        );
        finalWriter5.close();

        //task 6
        taskIndex++;
        PrintWriter finalWriter6 = SolveTasks.createFileWriter(taskIndex);
        List<String> filteredList = SolveTasks.filterActivities(listMonitoredData);
        finalWriter6.println("The activities that have more than 90% of the monitoring records with duration less than 5 minutes are the following:");
        filteredList.forEach(finalWriter6::println);
        finalWriter6.close();
    }
}
