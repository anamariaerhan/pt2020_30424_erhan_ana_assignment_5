import java.time.*;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;

public class MonitoredData {
    private String startTime;
    private String endTime;
    private String activityLabel;

    public MonitoredData(String startTime, String endTime, String activityLabel) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityLabel = activityLabel;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getDateWithoutTime() {
        return this.getStartTime().substring(0, this.getStartTime().indexOf(' '));
    }

    public DateTime getStartTime_DayTime() {
        DateTime date_start = DateTime.parse(this.startTime, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        return date_start;
    }

    public DateTime getEndTime_DayTime() {
        DateTime date_start = DateTime.parse(this.endTime, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        return date_start;
    }

    // joda-time library can work with Seconds
    public int getSecondsDurations() {
        return Seconds.secondsBetween(getStartTime_DayTime(), getEndTime_DayTime()).getValue(0); // int from seconds
    }

    // returns only the day, taken from a date format
    public int getDay() {
        String[] startTimeSplit = this.startTime.split("[ ]");
        String[] day = startTimeSplit[0].split("[-]");
        return Integer.parseInt(day[2]);
    }
}
