import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SolveTasks {
    /*task 1 - Read the data from the file Activity.txt using streams and split each line in 3 parts:
    start_time, end_time and activity_label, and create a list of objects of type MonitoredData*/
    public static List<MonitoredData> buildMonitoredDataList(List<String> stringList) {
        List<MonitoredData> activityList = new ArrayList<MonitoredData>();

        //split the read lines
        stringList.forEach((String line) -> {
            Pattern pattern = Pattern.compile("\t\t");
            Stream<String> dataFromLine = pattern.splitAsStream(line);

            // Create an ArrayList of the split data elements
            ArrayList<String> arrayList = dataFromLine.collect(Collectors.toCollection(ArrayList::new));
            MonitoredData newItem = new MonitoredData(arrayList.get(0), arrayList.get(1), arrayList.get(2).trim());
            activityList.add(newItem);
        });

        return activityList;
    }

    //task 2 - count nr of distinct days in the monitoring data
    public static int nrDifferentDays(List<MonitoredData> monitoredDataList) {
        List<String> distinctDaysMonitoringData = monitoredDataList.stream()
                .map(MonitoredData::getDateWithoutTime)
                .collect(Collectors.toList());

        List<String> distinct = distinctDaysMonitoringData.stream().distinct().collect(Collectors.toList());
        return distinct.size();
    }

    //task 3 - count the nr of occurrences for each activity over the entire monitoring data
    public static Map<String, Integer> activityOccurrence(List<MonitoredData> monitoredDataList) {
        Map<String, Integer> activityOccurrenceWithinMonitoringTime = monitoredDataList.stream().collect(Collectors
                .groupingBy(MonitoredData::getActivityLabel, Collectors.summingInt(a -> 1)));
        return activityOccurrenceWithinMonitoringTime;
    }

    // task 4 - Count for how many times each activity has appeared for each day over the monitoring period.
    public static Map<Integer, Map<String, Integer>> dailyAppearanceActivity(List<MonitoredData> monitoredDataList) {
        Map<Integer, Map<String, Integer>> mapping = monitoredDataList.stream().collect(Collectors
                .groupingBy(MonitoredData::getDay, Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingInt(a -> 1))));
        return mapping;
    }

    // task 5 - total duration of each activity
    public static Map<String, Integer> getIntFromSeconds(List<MonitoredData> monitoredDataList) {
        return monitoredDataList.stream().collect(Collectors
                .groupingBy(MonitoredData::getActivityLabel, Collectors.summingInt(MonitoredData::getSecondsDurations)));
    }

    // task 6 - Filter the activities that have more than 90% of the monitoring records with duration less than 5 minutes
    public static List<String> filterActivities(List<MonitoredData> listMonitoredData) {
        Map<String, Integer> filteredMap = (Map<String, Integer>) listMonitoredData.stream()
                .filter(e -> e.getSecondsDurations() < 300) // nr of seconds equivalent to 5 minutes: 5 * 60
                .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingInt(a -> 1)));

        Map<String, Integer> numberOccurrences = activityOccurrence(listMonitoredData);
        List<String> filteredList = new ArrayList<String>();
        filteredMap.entrySet().stream()
                .filter(e -> e.getValue() >= 0.9 * numberOccurrences.get(e.getKey()))
                .forEach(e -> filteredList.add(e.getKey()));
        return filteredList;
    }

    public static PrintWriter createFileWriter(int taskIndex) {
        String taskFilename = "Task_" + taskIndex + ".txt";
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(taskFilename, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return writer;
    }
}
